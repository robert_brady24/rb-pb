.global main
.func main
   
main:
    BL  _scanf              @ Branch to scan the first integer
    MOV R4, R0              @ Move integer1 to register 6

    BL  _scanf              @ Branch to scan the second integer
    MOV R5, R0              @ Move integer2 to register 5

    MOV R1, R4              @ Move integer1 to arg1 for GCD function
    MOV R2, R5              @ Move integer2 to arg2 for the GCD function
    BL _GCD                 @ Find the greatest common denominator for R1 and R2
    MOV R6, R0              @ Move the greatest common denominator result to R6
   
    MOV R1, R4              @ (Arg1) User's Input Integer1
    MOV R2, R5 		    @ (Arg2) User's Input Integer2
    MOV R3, R6              @ (Arg3) Greatest Common Denominator
    BL _printf		    @ Print "The GCD of %R1 and %R2 is %R3\n"

    B main                  @ Call main forever
 
_printf:
    PUSH {LR}               @ store the return address
    LDR R0, =printf_str     @ R0 contains formatted string address
    BL printf               @ call printf
    POP {PC}                @ restore the stack pointer and return
   
_scanf:
    PUSH {LR}               @ store the return address
    PUSH {R1}               @ backup regsiter value
    LDR R0, =format_str     @ R0 contains address of format string
    SUB SP, SP, #4          @ make room on stack
    MOV R1, SP              @ move SP to R1 to store entry on stack
    BL scanf                @ call scanf
    LDR R0, [SP]            @ load value at SP into R0
    ADD SP, SP, #4          @ remove value from stack
    POP {R1}                @ restore register value
    POP {PC}                @ restore the stack pointer and return

_modulus:
    PUSH {LR}		    @ push the return address
    CMP R1, R2 		    @ compare R1 to R2	
    MOVLT R0, R1            @ if (R1 < R2) return R1
    POPLT {PC} 		    @ return back to calling function

    SUB R1, R1, R2	    @ (Arg1)R1 = R1 - R2
    MOV R2, R2		    @ (Arg2)R2	
    BL _modulus		    @ call modulus(R1 - R2, R2)
    POP {PC}		    @ return back to calling function

_GCD:
    PUSH {LR}		    @ push the return address
    CMP R2, #0		    @ compare R2 to 0
    MOVEQ R0, R1	    @ if (R2 == 0) return R1
    POPEQ {PC}		    @ return back to calling function

    PUSH {R2}		    @ push R2 because _modulus overwrites it
    BL _modulus		    @ result = R1 % R2
    POP {R1}		    @ get back R2 from the stack
    MOV R2, R0 		    @ R2 = R1 % R2

    BL _GCD		    @ recursively call _GCD(R2, R1 % R2)	
    POP {PC}                @ return back to calling function
 
.data
number:         .word       0
format_str:     .asciz      "%d"
printf_str:     .asciz      "The GCD of %d and %d is %d\n"